package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
