package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;

public class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable UserDTO user) {
        super(user);
    }

}
